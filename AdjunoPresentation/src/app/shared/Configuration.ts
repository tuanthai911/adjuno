import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class Configuration {
    apiurl: string = 'https://localhost:44361/api/';

    onLoadingSpin(){
      document.getElementById("page-content").className += " loading";
      document.getElementById("loading-space").style.display = "block";
    }

    offLoadingSpin(){
      setTimeout( () => { 
        document.getElementById("page-content").classList.remove("loading");
        document.getElementById("loading-space").style.display = "none";
      }, 500 );
    }
}
