import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import {FooterComponent} from './components/footer/footer.component';
import {HeaderComponent} from './components/header/header.component';
import {MessagesComponent} from './components/message/messages.component';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {PageNotFoundComponent} from './pages/page-not-found/page-not-found.component';
import {InternalServerErrorComponent} from './pages/internal-server-error/internal-server-error.component';
import {ToastComponent} from './pages/toast/toast.component';

@NgModule({
    imports: [
      CommonModule,
      RouterModule,
      FormsModule,
      NgbAlertModule
    ],
    declarations: [
      FooterComponent,
      HeaderComponent,
      MessagesComponent,
      HomePageComponent,
      PageNotFoundComponent,
      InternalServerErrorComponent,
      ToastComponent
    ],
    exports: [
      FooterComponent,
      HeaderComponent,
      MessagesComponent,
      HomePageComponent,
      PageNotFoundComponent,
      InternalServerErrorComponent,
      ToastComponent
    ]
  })

  export class SharedModule {
  }
