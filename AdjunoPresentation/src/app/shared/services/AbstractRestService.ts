import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export class AbstractRestService<T> {

  private _value: T;
  private _type: Function;

  constructor(protected _http: HttpClient,
    protected actionUrl: string,
    private messageService: MessageService,
    type: Function
  ) {
    this._type = type;
  }

  get(url: string): Observable<T[]> {
    return this._http.get(url).pipe(
      tap(_ => this.logSuccess('get'))
    ) as Observable<T[]>;
  }

  getAll(): Observable<T[]> {
    return this._http.get(this.actionUrl).pipe(
      tap(_ => this.logSuccess('getAll'))
    ) as Observable<T[]>;
  }

  getOne(id: number): Observable<T> {
    return this._http.get(`${this.actionUrl}${id}`).pipe(
      tap(_ => this.logSuccess('getOne'))
    ) as Observable<T>;
  }

  update(obj: T): Observable<T> {
    return this._http.put(this.actionUrl, obj, httpOptions).pipe(
      tap(_ => this.logSuccess('update'))
    ) as Observable<T>;
  }

  add(obj: T): Observable<T> {
    return this._http.post<T>(this.actionUrl, obj, httpOptions).pipe(
      tap(_ => this.logSuccess('add'))
    ) as Observable<T>;
  }

  private logSuccess(operation = 'operation') {
    this.messageService.add(`${this._type.prototype.constructor.name} ${operation} success`, 'success');
    this.offLoaddingSpinner();
  }

  private offLoaddingSpinner() {
    setTimeout(() => {
      document.getElementById("page-content").classList.remove("loading");
      document.getElementById("loading-space").style.display = "none";
    }, 100);
  }
}
