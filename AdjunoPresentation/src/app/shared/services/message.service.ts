import { Injectable } from '@angular/core';

class Alert {
  type: string;
  message: string;
}

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  alerts: Alert[] = [];

  add(message: string, type: string) {
    this.alerts.push({
      type: type,
      message: message
    });
  }

  close(alert: Alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }
}
