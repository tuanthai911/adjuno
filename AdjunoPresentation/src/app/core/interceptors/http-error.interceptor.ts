import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MessageService } from '../../shared/services/message.service';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private router: Router, private messageService: MessageService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(error => {

      if (error.status === 401) {
        // unauthorized
      } else if (error.status === 500) {
        this.handle500Error(error);
      } else if (error.status === 404) {
        this.handle404Error(error);
      } else {
        this.handleOtherError(error);
      }

      this.offLoaddingSpinner();

      return throwError(error);
    }) as any) as any;
  }

  private handle500Error(error: HttpErrorResponse) {
    this.logError(error.error.errorMessage);
    this.router.navigate(['/500']);
  }

  private handle404Error(error: HttpErrorResponse) {
    this.logError(error.error.errorMessage);
    this.router.navigate(['/404']);
  }

  private handleOtherError(error: HttpErrorResponse) {
    this.logError('Server is shutdown');
  }

  /** Log a HeroService message with the MessageService */
  private logError(message: string) {
    this.messageService.add(message, 'danger');
  }

  private offLoaddingSpinner() {
    setTimeout(() => {
      document.getElementById("page-content").classList.remove("loading");
      document.getElementById("loading-space").style.display = "none";
    }, 100);
  }
}


