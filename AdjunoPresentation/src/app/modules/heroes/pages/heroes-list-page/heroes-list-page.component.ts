import { Component, OnInit } from '@angular/core';
import { Hero } from '../../shared/hero.model';
import { HeroService } from '../../shared/hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes-list-page.component.html',
  styleUrls: ['./heroes-list-page.component.css']
})
export class HerosComponent implements OnInit {

  constructor(private heroService: HeroService) { }

  heroes: Hero[];

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes().subscribe(heroes => this.heroes = heroes);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.addHero({ name } as Hero)
      .subscribe(hero => {
        this.heroes.push(hero);
      });
  }

  delete(hero: Hero): void {
    this.heroes = this.heroes.filter(h => h !== hero);
    this.heroService.deleteHero(hero).subscribe();
  }

}
