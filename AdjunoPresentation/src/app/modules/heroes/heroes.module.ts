import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import {HeroRoutingModule} from './heroes-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {HerosComponent} from './pages/heroes-list-page/heroes-list-page.component';
import {HeroDetailComponent} from './pages/heroes-detail-page/heroes-detail-page.component';
import {DashboardComponent} from './pages/hero-dashboard-page/dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FormsModule,
    HeroRoutingModule
  ],
  declarations: [
    HerosComponent,
    HeroDetailComponent,
    DashboardComponent
  ]
})

export class HeroesModule {
}
