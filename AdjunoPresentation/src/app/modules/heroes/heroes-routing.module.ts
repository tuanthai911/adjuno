import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HerosComponent} from './pages/heroes-list-page/heroes-list-page.component';
import {HeroDetailComponent} from './pages/heroes-detail-page/heroes-detail-page.component';

const heroesRoutes: Routes = [
    { path: '', component: HerosComponent },
    { path: ':id', component: HeroDetailComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(heroesRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class HeroRoutingModule {
}
