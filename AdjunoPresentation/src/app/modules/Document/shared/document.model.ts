export class Document {
    DocumentId: number;
    Name: string;
    Description: string;
    CreatedDate: Date;
    IsNew: boolean;
    IsAcknowledge: boolean;
    DocumentKind: string;
    DocumentDefaultLink: string;
}
