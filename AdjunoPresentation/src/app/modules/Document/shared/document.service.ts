import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

import { AbstractRestService } from '../../../shared/services/AbstractRestService';
import { Document } from './document.model';
import { Configuration } from '../../../shared/Configuration';
import { MessageService } from '../../../shared/services/message.service';

@Injectable({
  providedIn: 'root'
})
export class DocumentService extends AbstractRestService<Document> {
  constructor(private httpClient: HttpClient,
    private configuration: Configuration,
    private message: MessageService)
    {
    super(httpClient, configuration.apiurl + 'Document/', message, Document);
  }

  getDocumentByUserId(userId: number): Observable<Document[]> {
    return this.get(this.configuration.apiurl + 'Document/GetByUser/' + userId);
  }
}
