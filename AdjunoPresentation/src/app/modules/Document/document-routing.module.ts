import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DocumentDashboardComponent} from './pages/document-dashboard/document.dashboard';

const documentsRoutes: Routes = [
    { path: '', component: DocumentDashboardComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(documentsRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class DocumentRoutingModule {
}
