import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { DocumentRoutingModule } from './document-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { DocumentDashboardComponent } from './pages/document-dashboard/document.dashboard';
import { DocumentAddDialogComponent } from './pages/document-add-dialog/document-add-dialog';
import { DocumentImageComponent } from './pages/document-image-view/document-image-view';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FormsModule,
    DocumentRoutingModule,
  ],
  declarations: [
    DocumentDashboardComponent,
    DocumentAddDialogComponent,
    DocumentImageComponent
  ]
})

export class DocumentModule {
}
