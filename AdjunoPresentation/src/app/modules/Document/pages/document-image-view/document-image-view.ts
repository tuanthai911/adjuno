import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { Document } from '../../shared/document.model';

@Component({
    selector: 'app-document-image',
    templateUrl: './document-image-view.html',
    styleUrls: ['./document-image-view.css']
})
export class DocumentImageComponent implements OnInit {

    constructor(private sanitizer: DomSanitizer) { }

    @Input() document: Document;

    ngOnInit() {
    }

    photoURL(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
}
