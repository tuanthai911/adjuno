import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

import { Document } from '../../shared/document.model';
import { DocumentService } from '../../shared/document.service';
import { Configuration } from '../../../../shared/Configuration';

@Component({
  selector: 'app-dashboard',
  templateUrl: './document.dashboard.html'
})
export class DocumentDashboardComponent implements OnInit {
  documents: Document[] = [];

  constructor(private documentService: DocumentService,
    private sanitizer: DomSanitizer,
    private configuration: Configuration) { }

  ngOnInit() {
    this.configuration.onLoadingSpin();
    this.getDocuments();
  }

  getDocuments(): void {
    this.documentService.getDocumentByUserId(1)
      .subscribe(documents => this.documents = documents);
  }

  photoURL(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
