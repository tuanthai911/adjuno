import { Component, Inject, OnInit } from '@angular/core';

import { Document } from '../../shared/document.model';
import { DocumentService } from '../../shared/document.service';

@Component({
    selector: 'app-document-add-dialog',
    templateUrl: 'document-add-dialog.html',
    styleUrls: ['./document-add-dialog.css']
})
export class DocumentAddDialogComponent implements OnInit {

    constructor(private documentService: DocumentService) { }

    document: Document = {} as Document;
    fileToUpload: File = null;
    fileAlertString: string = 'Drag your files here or click in this area.';

    ngOnInit() {
    }

    OnSubmit() {
        console.log(this.document);
        this.documentService.add(this.document)
            .subscribe(document => this.document = document);
    }

    handleFileInput(files: FileList) {
        console.log(files.item(0));
        this.fileToUpload = files.item(0);
        this.document.Name = this.fileToUpload.name;
        this.document.DocumentKind = this.fileToUpload.type;

        this.fileAlertString = this.fileToUpload.name;
    }
}
