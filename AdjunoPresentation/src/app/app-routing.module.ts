import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HomePageComponent} from './shared/pages/home-page/home-page.component';
import {PageNotFoundComponent} from './shared/pages/page-not-found/page-not-found.component';
import { InternalServerErrorComponent } from './shared/pages/internal-server-error/internal-server-error.component';

const routes: Routes = [
  {path: '', component: HomePageComponent, pathMatch: 'full'},
  {path: 'heroes', loadChildren: './modules/heroes/heroes.module#HeroesModule'},
  {path: 'document', loadChildren: './modules/Document/document.module#DocumentModule'},
  { path: '500', component: InternalServerErrorComponent },
  { path: '404', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/404' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled'
    })
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
}
