﻿using AdjunoApi.Models.Model;
using AdjunoApi.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdjunoApi.Repository.Implement
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(AdjunoContext context)
            : base(context)
        {

        }
    }
}
