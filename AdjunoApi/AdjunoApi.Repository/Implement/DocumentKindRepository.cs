﻿using AdjunoApi.Models.Model;
using AdjunoApi.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdjunoApi.Repository.Implement
{
    public class DocumentKindRepository : RepositoryBase<DocumentKind>, IDocumentKindRepository
    {
        public DocumentKindRepository(AdjunoContext context)
            : base(context)
        {

        }
    }
}
