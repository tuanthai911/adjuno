﻿using AdjunoApi.Models.Model;
using AdjunoApi.Repository.Contract;

namespace AdjunoApi.Repository.Implement
{
    public class DocumentRepository : RepositoryBase<Document>, IDocumentRepository
    {
        public DocumentRepository(AdjunoContext context)
            : base(context)
        {

        }
    }
}
