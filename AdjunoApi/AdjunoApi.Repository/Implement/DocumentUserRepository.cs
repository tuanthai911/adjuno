﻿using AdjunoApi.Models.Model;
using AdjunoApi.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdjunoApi.Repository.Implement
{
    public class DocumentUserRepository : RepositoryBase<DocumentUser>, IDocumentUserRepository
    {
        public DocumentUserRepository(AdjunoContext context)
            : base(context)
        {

        }
    }
}
