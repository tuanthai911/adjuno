﻿using AdjunoApi.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdjunoApi.Repository
{
    public interface IRepositoryWrapper
    {
        IRepositoryBase<T> GetRepository<T>() where T : BaseModel;
    }
} 
