﻿using AdjunoApi.Models.Model;
using Microsoft.EntityFrameworkCore;
using System;

namespace AdjunoApi.Repository
{
    public class AdjunoContext : DbContext
    {
        public AdjunoContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentUser> DocumentUser { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<User>().HasData(
        //        new User() { Id = 1, FirstName = "Tuan", LastName = "Thai", CreatedDate = DateTime.Now, DateOfBirth = DateTime.Now },
        //        new User() { Id = 2, FirstName = "Thi", LastName = "Le", CreatedDate = DateTime.Now, DateOfBirth = DateTime.Now });

        //    modelBuilder.Entity<Document>().HasData(
        //        new Document() { Id = 1, Name = "SH", Description = "Shrelock Holmes", CreatedDate = DateTime.Now },
        //        new Document() { Id = 2, Name = "SUN", Description = "Start up Nation", CreatedDate = DateTime.Now },
        //        new Document() { Id = 3, Name = "DV", Description = "Dung Viec", CreatedDate = DateTime.Now });

        //    modelBuilder.Entity<DocumentUser>().HasData(
        //        new DocumentUser() { Id = 1, DocumentId = 1, UserId = 1, CreatedDate = DateTime.Now, IsRead = true },
        //        new DocumentUser() { Id = 2, DocumentId = 2, UserId = 1, CreatedDate = DateTime.Now, IsRead = false },
        //        new DocumentUser() { Id = 3, DocumentId = 3, UserId = 1, CreatedDate = DateTime.Now, IsRead = true },
        //        new DocumentUser() { Id = 4, DocumentId = 1, UserId = 2, CreatedDate = DateTime.Now, IsRead = false },
        //        new DocumentUser() { Id = 5, DocumentId = 2, UserId = 2, CreatedDate = DateTime.Now, IsRead = false });
        //}
    }
}
