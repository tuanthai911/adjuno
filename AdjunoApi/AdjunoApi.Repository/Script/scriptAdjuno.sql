USE [AdjunoDB]
GO
SET IDENTITY_INSERT [dbo].[DocumentKind] ON 

INSERT [dbo].[DocumentKind] ([Id], [CreatedDate], [UpdatedDate], [Name], [DefaultLink]) VALUES (1, CAST(N'2001-01-01T00:00:00.0000000' AS DateTime2), NULL, N'Image', N'https://cdn.tgdd.vn/Files/2017/03/31/966931/iphone-6s_800x450.jpg')
INSERT [dbo].[DocumentKind] ([Id], [CreatedDate], [UpdatedDate], [Name], [DefaultLink]) VALUES (2, CAST(N'2001-01-01T00:00:00.0000000' AS DateTime2), NULL, N'Video', N'https://www.youtube.com/embed/9elEGm_g_D4')
SET IDENTITY_INSERT [dbo].[DocumentKind] OFF
SET IDENTITY_INSERT [dbo].[Documents] ON 

INSERT [dbo].[Documents] ([Id], [CreatedDate], [UpdatedDate], [Name], [Description], [KindOfDocument], [AzureLink]) VALUES (1, CAST(N'2001-01-01T00:00:00.0000000' AS DateTime2), NULL, N'IphoneX', N'This is iPhone', 1, NULL)
INSERT [dbo].[Documents] ([Id], [CreatedDate], [UpdatedDate], [Name], [Description], [KindOfDocument], [AzureLink]) VALUES (2, CAST(N'2001-01-01T00:00:00.0000000' AS DateTime2), NULL, N'Thuy Chi', N'Cho Ngay Anh Nhan Ra em', 2, NULL)
SET IDENTITY_INSERT [dbo].[Documents] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [CreatedDate], [UpdatedDate], [FirstName], [LastName], [DateOfBirth], [PhoneNumber], [Email]) VALUES (1, CAST(N'2001-01-01T00:00:00.0000000' AS DateTime2), NULL, N'Tuan', N'Thai', CAST(N'2001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
SET IDENTITY_INSERT [dbo].[DocumentUser] ON 

INSERT [dbo].[DocumentUser] ([Id], [CreatedDate], [UpdatedDate], [IsNew], [IsAcknowledge], [UserId], [DocumentId]) VALUES (1, CAST(N'2001-01-01T00:00:00.0000000' AS DateTime2), NULL, 1, 0, 1, 1)
INSERT [dbo].[DocumentUser] ([Id], [CreatedDate], [UpdatedDate], [IsNew], [IsAcknowledge], [UserId], [DocumentId]) VALUES (2, CAST(N'2001-01-01T00:00:00.0000000' AS DateTime2), NULL, 0, 1, 1, 2)
SET IDENTITY_INSERT [dbo].[DocumentUser] OFF
