﻿using AdjunoApi.Models;
using AdjunoApi.Repository.Contract;
using AdjunoApi.Repository.Implement;

namespace AdjunoApi.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private AdjunoContext _adjunoContext;
        private IUserRepository _user;
        private IDocumentRepository _document;
        private IDocumentUserRepository _documentUser;
        private IDocumentKindRepository _documentKind;


        public IRepositoryBase<T> GetRepository<T>() where T : BaseModel
        { 
            if (typeof(T).Name == "User")
            {
                if (_user == null)
                {
                    _user = new UserRepository(_adjunoContext);
                }

                return (IRepositoryBase<T>)_user;
            }
            else if (typeof(T).Name == "Document")
            {
                if (_document == null)
                {
                    _document = new DocumentRepository(_adjunoContext);
                }

                return (IRepositoryBase<T>)_document;
            }
            else if (typeof(T).Name == "DocumentUser")
            {
                if (_documentUser == null)
                {
                    _documentUser = new DocumentUserRepository(_adjunoContext);
                }

                return (IRepositoryBase<T>)_documentUser;
            }
            else if (typeof(T).Name == "DocumentKind")
            {
                if (_documentKind == null)
                {
                    _documentKind = new DocumentKindRepository(_adjunoContext);
                }

                return (IRepositoryBase<T>)_documentKind;
            }

            return null;
        }

        public RepositoryWrapper(AdjunoContext adjunoContext)
        {
            _adjunoContext = adjunoContext;
        }
    }
}
