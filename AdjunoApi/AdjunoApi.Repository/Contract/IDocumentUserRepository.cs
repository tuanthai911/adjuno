﻿using AdjunoApi.Models.Model;

namespace AdjunoApi.Repository.Contract
{
    public interface IDocumentUserRepository : IRepositoryBase<DocumentUser>
    {
        
    }
}
