﻿using AdjunoApi.Models.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdjunoApi.Repository.Contract
{
    public interface IUserRepository : IRepositoryBase<User>
    {
    }
}
