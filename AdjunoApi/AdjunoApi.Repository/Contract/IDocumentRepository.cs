﻿using AdjunoApi.Models.Model;

namespace AdjunoApi.Repository.Contract
{
    public interface IDocumentRepository : IRepositoryBase<Document>
    {
        
    }
}
