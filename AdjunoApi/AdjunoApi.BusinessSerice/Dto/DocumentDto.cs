﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdjunoApi.BusinessSerice.Dto
{
    public class DocumentDto
    {
        public int DocumentId { get; set; } 
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsNew { get; set; }
        public bool IsAcknowledge { get; set; } 
        public string DocumentKind { get; set; }
        public string DocumentDefaultLink { get; set; } 
    }
}
