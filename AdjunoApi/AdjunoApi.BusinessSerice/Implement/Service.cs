﻿using AdjunoApi.BusinessSerice.Contract;
using AdjunoApi.Models;
using AdjunoApi.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AdjunoApi.BusinessSerice.Implement
{
    public abstract class Service<T> : IService<T> where T : BaseModel
    {
        protected IRepositoryWrapper _repoWrapper;

        public Service(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        protected IRepositoryBase<T> GetRepository()
        {
            return _repoWrapper.GetRepository<T>();
        }

        public Task<T> Add(T entity)
        {
            GetRepository().Add(entity);
            GetRepository().Save();
            return Get(entity.Id);
        }

        public void Delete(T entity)
        {
            GetRepository().Delete(entity);
        }

        public Task<T> Get(int id)
        {
            return GetRepository().FindBy(x=>x.Id == id);
        }

        public Task<List<T>> GetAll()
        {
            return GetRepository().GetAll();
        }

        public void Update(T entity)
        {
            GetRepository().Update(entity);
        }
    }
}
