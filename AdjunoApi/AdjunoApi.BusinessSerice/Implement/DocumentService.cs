﻿using AdjunoApi.BusinessSerice.Contract;
using AdjunoApi.BusinessSerice.Dto;
using AdjunoApi.Models.Model;
using AdjunoApi.Repository;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdjunoApi.BusinessSerice.Implement
{
    public class DocumentService : Service<Document>, IDocumentService
    {
        private readonly IMapper _mapper;
        public DocumentService(IRepositoryWrapper repoWrapper, IMapper mapper)
            : base(repoWrapper)
        {
            _mapper = mapper;
        }

        public async Task<DocumentDto> GetDocumentById(int id)
        {
            return _mapper.Map<DocumentDto>(await Get(id));
        }

        public async Task<List<DocumentDto>> GetDocumentByUserId(int userId)
        {
            var documents = await GetRepository().SearchBy(x => x.DocumentUser.Any(y => y.UserId == userId), x => x.DocumentUser, x=>x.DocumentKind);
            return _mapper.Map<List<DocumentDto>>(documents);
        }

        public async Task<DocumentDto> AddDocument(DocumentDto dto)
        {
            var document = new Document();
            try
            {
                var documentKind = new DocumentKind();
                documentKind.Name = dto.Name.Substring(dto.Name.IndexOf('.') + 1);
                await _repoWrapper.GetRepository<DocumentKind>().Add(documentKind);
                _repoWrapper.GetRepository<DocumentKind>().Save();

                document.Name = dto.Name.Substring(0, dto.Name.IndexOf('.'));
                document.Description = dto.Description;
                document.KindOfDocument = documentKind.Id;
                document = await Add(document);

                var documentUser = new DocumentUser();
                documentUser.UserId = 1;
                documentUser.DocumentId = document.Id;
                documentUser.IsNew = true;
                documentUser.IsAcknowledge = false;
                await _repoWrapper.GetRepository<DocumentUser>().Add(documentUser);
                _repoWrapper.GetRepository<DocumentUser>().Save();

            }
            catch(Exception e)
            {

            }
            return _mapper.Map<DocumentDto>(document);
        }
    }
}
