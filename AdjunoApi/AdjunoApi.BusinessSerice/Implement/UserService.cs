﻿using AdjunoApi.BusinessSerice.Contract;
using AdjunoApi.Models.Model;
using AdjunoApi.Repository;

namespace AdjunoApi.BusinessSerice.Implement
{
    public class UserService : Service<User>, IUserService
    {
        public UserService(IRepositoryWrapper repoWrapper)
            :base(repoWrapper)
        {

        }
    }
}
