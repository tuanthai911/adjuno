﻿using AdjunoApi.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AdjunoApi.BusinessSerice.Contract
{
    public interface IService<T> where T : BaseModel
    {
        Task<List<T>> GetAll();
        Task<T> Get(int id);
        Task<T> Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
