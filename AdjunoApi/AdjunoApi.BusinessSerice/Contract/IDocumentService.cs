﻿using AdjunoApi.BusinessSerice.Dto;
using AdjunoApi.Models.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdjunoApi.BusinessSerice.Contract
{
    public interface IDocumentService : IService<Document>
    {
        Task<DocumentDto> GetDocumentById(int id);
        Task<List<DocumentDto>> GetDocumentByUserId(int userId);
        Task<DocumentDto> AddDocument(DocumentDto dto);
    }
}
