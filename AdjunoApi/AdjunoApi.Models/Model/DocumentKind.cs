﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdjunoApi.Models.Model
{
    public class DocumentKind : BaseModel
    {
        public string Name { get; set; }
        public string DefaultLink { get; set; } 
    }
}
