﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdjunoApi.Models.Model
{
    public class Document : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int KindOfDocument { get; set; }
        public string AzureLink { get; set; } 

        public virtual List<DocumentUser> DocumentUser { get; set; }
        [ForeignKey("KindOfDocument")]
        public virtual DocumentKind DocumentKind { get; set; }
    }
}
