﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdjunoApi.Models.Model
{
    public class User : BaseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PhoneNumber { get; set; } 
        public string Email { get; set; }
        public virtual List<DocumentUser> DocumentUser { get; set; } 
    }
}
