﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdjunoApi.Models.Model
{
    public class DocumentUser : BaseModel
    {
        public bool IsNew { get; set; }
        public bool IsAcknowledge { get; set; }

        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; } 
        
        public int DocumentId { get; set; }
        [ForeignKey("DocumentId")]
        public virtual Document Document { get; set; }
    }
}
