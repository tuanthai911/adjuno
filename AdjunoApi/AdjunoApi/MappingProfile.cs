﻿using AdjunoApi.BusinessSerice.Dto;
using AdjunoApi.Models.Model;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdjunoApi
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<Document, DocumentDto>()
                .ForMember(otp => otp.IsNew, dest => dest.MapFrom(x=>x.DocumentUser[0].IsNew))
                .ForMember(otp => otp.DocumentId, dest => dest.MapFrom(x => x.Id))
                .ForMember(otp => otp.CreatedDate, dest => dest.MapFrom(x => x.CreatedDate.Date))
                .ForMember(otp => otp.IsAcknowledge, dest => dest.MapFrom(x => x.DocumentUser[0].IsAcknowledge))
                .ForMember(otp => otp.DocumentKind, dest => dest.MapFrom(x => x.DocumentKind.Name))
                .ForMember(otp => otp.DocumentDefaultLink, dest => dest.MapFrom(x => x.DocumentKind.DefaultLink));

            CreateMap<DocumentDto, Document>()
                .ForMember(otp => otp.Id, dest => dest.MapFrom(x => x.DocumentId));
        }
    }
}
