﻿using AdjunoApi.BusinessSerice.Contract;
using AdjunoApi.BusinessSerice.Dto;
using AdjunoApi.Models.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdjunoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : ControllerBase
    {
        private IDocumentService _documentService;

        public DocumentController(IDocumentService documentService)
        {
            _documentService = documentService;
        }
        // GET api/document 
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Document>>> Get()
        {
            return await _documentService.GetAll();
        }

        // GET api/document/5
        [HttpGet("Get/{id}")]
        public async Task<ActionResult<DocumentDto>> Get(int id)
        {
            return await _documentService.GetDocumentById(id);
        }

        [HttpGet("GetByUser/{userId}")]
        public async Task<ActionResult<List<DocumentDto>>> GetByUser(int userId)
        {
            //return StatusCode(500);
            return await _documentService.GetDocumentByUserId(userId);
        }

        // POST api/document
        [HttpPost]
        public async Task<ActionResult<DocumentDto>> Post([FromBody] DocumentDto dto)
        {
            return await _documentService.AddDocument(dto);
        }

        // PUT api/document/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/document/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
